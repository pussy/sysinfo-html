#!/bin/bash
# test script
#-----------------------------------------------------------------------------------------------#
#																								#
#		Shell script to collect basic information about a workstation 							#
#		Save the output as html file.															#
#		Tested only with RHEL 6.6 and RHEL 6.7													#
#																								#
#-----------------------------------------------------------------------------------------------#



#CONSONANTS
TITLE="System Information for"	
RIGHT_NOW=$(date +"%A %F %t %T %:z")
TIME_STAMP="Updated by $(logname) on $RIGHT_NOW (Script was run as $USER) "
#END OF CONSONANTS




#FUNCTIONS


#verify_user()
#{


#}

show_systeminfo()
# System release info like OS, Kernel e.t.c.
{
echo "<h2>System info</h2>"
echo "<pre>"
echo -n "Machine name		"
uname -n
echo -n "Kernel		"
uname -sr
# Find any release files in /etc
if ls /etc/*release 1>/dev/null 2>&1; then
	echo "Release info	"
	for i in /etc/*release; do
		head -n 2 $i
	done
fi
echo "</pre>"
echo "<br><br>"
}

# System uptime
show_uptime()
{
echo "<h2>System Uptime</h2>"
echo "<pre>"
uptime
echo "</pre>"
echo "<br><br>"
}

#Storage space status
show_drivespace()
{
echo "<h2>Storage space status</h2>"
echo "<pre>"
df
echo "</pre>"
echo "<br><br>"
}



#Storage status by user a/c
#only su can get this info
show_homespace()
{

	echo "<h2>Files and storage used by the home directory</h2>"
	echo "<pre>"
	format="%10s	%10s	%10s	%-s	\n"
	printf "$format" "Dirs" "Files" "Blocks" "Path"
	if [ $(id -u) = "0" ]; then
		dir_list="/home/*"
	else
		dir_list=$HOME
	fi
	for home_dir in $dir_list; do 
		total_dirs=$(find $home_dir -type d | wc -l)
		total_files=$(find $home_dir -type f | wc -l)
        	total_blocks=$(du -s $home_dir)
       		printf "$format" $total_dirs $total_files $total_blocks
	done

	echo "</pre>"

	if [ $(id -u) != "0" ]; then
		echo "<p>"
		echo "Home directory of current user is only displayed since the script was run without super user privilege. Run this script as super user to get info about all the users in this Machine. " 
		echo "</p>"
	fi
	echo "<br><br>"
}




#Show memory and swap status
show_memory()
{
#work in progress
echo "<h2> Memory and Swap in use </h2 >"
echo  "<pre>"
free
echo "</pre>"
echo "<br><br>"
}



#Show ip address
show_ipaddr()
{
#work in progress
echo "<h2> IP address </h2 >"
echo "<pre>"
echo -n " The eth0 h/w IPv4 address is 	"
ifconfig | awk '/inet addr/{print $2}' | cut -d: -f2 | grep -v '0.0' | head -1
echo  ""
echo -n " The device hardware address is 	"
ifconfig | awk '/HWaddr/{print $5}' | head -1
echo  ""
#ip -o  addr
echo "</pre>"
echo "<br><br>"
}






#help info or manual
usage()
{
echo "Help"
echo "#---------------------------------------------------------------------------------------#"
echo "#											#"
echo "#	Shell script to collect basic information about a workstation 			#
#	Save the output as html file.							#
#	Should work on most GNU/Linux distros						#
#	tested only with RHEL 6.6 and RHEL 6.7						#
#	It saves the output file named \"sysinfo.html\" in desktop			#
#											#
#											#
#---------------------------------------------------------------------------------------#
"
echo ""

}



#To print whether script was executed succesfully or not
#feature not implemented
##scriptexitstatus()
##{
##if [ $? == "0" ];
##then
##	echo -n "Executed succesfully. Output saved as	" 
##	echo $filename
##else
##	echo "Exited with error code $?"
##fi
##}






#save output as html
write_page()
{
cat <<- _EOF_
	<html>
	<head>
		<title>
		$TITLE $HOSTNAME
		</title>
	</head>

	<body>
	<h1>$title</h1>
	<p>$TIME_STAMP</p>
	$(show_systeminfo)
	$(show_uptime)
	$(show_drivespace)
	$(show_homespace)
	$(show_memory)
	$(show_ipaddr)
	</body>
	</html>
_EOF_
}


#END OF FUNCTIONS


#INTERACTIVE, Verbose and Sundry
interactive=
if [ $(id -u) = "0" ];
then
	filename=/home/$SUDO_USER/Desktop/sysinfo.html
else
	filename=~/Desktop/sysinfo.html
fi

#Notice about output
print_notice()
{
if [ "$1" = "" ];
then 
	echo "Output file will be saved as $filename 
Existing file, if any will be overwritten"
fi
}
print_notice

while [ "$1" != "" ]; do
	case $1 in
		-f | --file )		shift
					filename=$1
					;;
		-i | --interactive )	interactive=1
					;;
		-h | --help )		usage
					exit
					;;
		* )			usage
					exit 1
	esac
	shift
done

if [ "$interactive" = "1" ]; then
	response=
	echo -n "Enter path and name of the output file [$filename]  "
	read response
	if [ -n "$response" ]; then
        	filename=$response
	fi
	if [ -f $filename ]; then
		echo -n "Output file exists. Overwrite? (y/n)  "
        	read response
        	if [ "$response" != "y" ]; 
			then
				echo "Output not saved."
				exit 1
			else
				echo "Output file will be saved as $filename 
				Existing file, if any will be overwritten"
			fi
	fi
fi




#END OF INTERACTIVE, Verbose and Sundry



#MAIN

write_page > $filename

#END OF MAIN
